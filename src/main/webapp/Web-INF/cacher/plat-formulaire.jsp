	<jsp:include page="/Web-INF/cacher/head.jsp" />
	
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
	

	<body>
	
<h1>Nouveau Plat</h1>
     <p>${message}</p>
     
	<form method='post' autocomplete="on"> 
		<fieldset>
		<p><label> Nom du plat <input type="text" name="nom" value="${nom}" required></label></p>
		<p><label> Description <textarea name="description" cols="20" rows="5" required></textarea></label></p>
		<p><label> Classification <select name="classification"> 
						<option value="Plat principale">Plat principale</option>
				        <option value="Entree">Entrée</option>
				        <option value="Dessert">Dessert</option>
				        <option value="Apperitif">Appéritif</option></select></label></p>
        <p><label>Prix <input type="number" name="prix" step="0.01" required /> Euro</label></p>        	
		
		<p><input type="submit"></p>
		</fieldset>
	</form>
	
	<h2> Liste complete</h2>
	<table> 
            <tr><th>Nom</th><th>Description</th><th>Classification</th><th>Prix</th></tr>

            <c:forEach items="${platC}" var="p">
                        <tr>
                <td>${p.nom}<br></td>
                <td>${p.description}<br></td>
                <td>${p.classification}<br></td>
               <td><fmt:formatNumber value="${p.prix}" 
								minFractionDigits="2" maxFractionDigits="2" /> Euro</td>
			   <td><a href="/remove?id=${p.id}">Supprimer</a></td>
                </tr>
            </c:forEach>
            
     </table>
	</body>
</html>