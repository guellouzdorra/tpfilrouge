
	<jsp:include page="/Web-INF/cacher/head.jsp" />
	
		<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

	<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
	

	<body>
		<h1>Reservation</h1>
   <p>${message}</p>
	<form method='post' autocomplete="on"> 
		<fieldset>
        <p><label for="date">Date  </label><input type="date" name="date" id="date"></p><br> 
        <p><label for="horaire" >Horaire  </label><input type="text" name="horaire" id="horaire"></p><br> 
        <p><label for="nom" >Nom  </label><input type="text" name="nom" id="nom"></p><br>
        <p><label for="nombreConvives" >Nombre de Convives  </label><input type="number" name="nombreConvives" id="nombreConvives"></p><br> 
		<p><input type="button" id="envoyer" value="Envoyer"></p>
		

		</fieldset>
	</form>
<table id="taRes"></table>
<script type="text/javascript">
let date = document.getElementById("date")
let horaire = document.getElementById("horaire")
let nom = document.getElementById("nom")
let nombreConvives = document.getElementById("nombreConvives")
let taRes = document.getElementById("taRes")
let envoyer = document.getElementById("envoyer")



envoyer.addEventListener("click",function(){
	envoyer.disabled =true

	console.log("il y a un click")
	let xmlhttp = new XMLHttpRequest()
	let theUrl = "http://localhost:8081/json/reservation";
	xmlhttp.open("POST", theUrl);
	xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
	xmlhttp.onreadystatechange = function(){
		if(this.status = 200 && this.readyState==4){
			alert ("Reponse recue : " + this.responseText)
			let reservations = JSON.parse(this.responseText)
			let html = "<tr><th>Nom</th><th>Nombres de Convives</th><th>Date</th><th>Heure</th></tr>"
			for(let reservation of reservations) {
			       html += "<tr><td>"+reservation["nom"]+"</td>"+
                   "<td>"+reservation["nombreConvives"]+"</td>"+
                   "<td>"+reservation["date"]+"</td>"+
                   "<td>"+reservation["horaire"]+"</td>"+
			      // "<td>"+"<a href='supprime?id="+reservation["id"]+"'>Supprimer</a>"+"</td>"+ 
			      
            "<td>"+"<input type='button' onclick='supprimer(" +reservation["id"]+ ")' value='Supprimer'>"+"</td>"+

                   "</tr>"
			}
			taRes.innerHTML = html
		}
	}
	xmlhttp.send(JSON.stringify({"nom" : nom.value, "date":date.value, "horaire":horaire.value, "nombreConvives":nombreConvives.value } ));
})


function supprimer(id){
let xmlhttp = new XMLHttpRequest()
	let theUrl = "http://localhost:8081/supprime?id="+id;
	xmlhttp.open("POST", theUrl);
	xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
	xmlhttp.send();
	}
	
	
	

</script>
	
	</body>
</html>
