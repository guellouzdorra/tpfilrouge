package fr.ib.dorra.Cuisine;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Service 

   public class PlatService {
	
       private SessionFactory sessionFactory;   		
   
	    public void enregistrerFormulaire(String nom, String description, String classification, float prix) {  
	    	
	      	Session session = sessionFactory.openSession();  
	   		
	   		Transaction tx = session.beginTransaction();
	   		
	   		PlatCuisine p1= new PlatCuisine (nom, description, classification, prix);
	   		session.save(p1);
	   		
	   		tx.commit();
	   		session.close();
   	   
	   	}
	    	 
	    public void afficher(Model model) {  
  
	    	Session session = sessionFactory.openSession();  
	   		Transaction tx = session.beginTransaction();

			List<PlatCuisine> platCuisine = session.
					createQuery("from PlatCuisine order by classification, nom", PlatCuisine.class).list();  
			
			model.addAttribute("platC", platCuisine); 
			
			session.close();		   
		}
	   
	    public void supprimerRecette(int id) {
			Session session = sessionFactory.openSession();
			Transaction tx = session.beginTransaction();
			
			try {
				PlatCuisine plat = session.load(PlatCuisine.class, id);
				session.delete(plat);
				tx.commit();
				
			} catch (Exception e) {
				System.err.println("Erreur : "+e);
				tx.rollback();
			}
			session.close();
		}

	    @Autowired
	   	public void setSessionFactory(SessionFactory sessionFactory) {
	   		this.sessionFactory = sessionFactory;
	   	}
    }



