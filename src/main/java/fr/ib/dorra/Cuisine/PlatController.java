package fr.ib.dorra.Cuisine;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class PlatController {
	private static final Logger logger = LoggerFactory.getLogger(PlatController.class);
	private SessionFactory sessionFactory;
    private PlatService platService;
		
		
		//rentrer les donnees de formulaire
		@RequestMapping(path="/plat", method=RequestMethod.GET)
		public String afficherFormulaire(Model model) {
			platService.afficher(model);
			
			return "plat-formulaire";
		}
		
		//recevoir les données de formulaire
		@RequestMapping(path="/plat", method=RequestMethod.POST)
		public String traiterFormulaire(Model model,
				@RequestParam("nom") String nom,     //en bleu: le contenu de case
				@RequestParam("description") String description,
				@RequestParam("classification") String classification, 
				@RequestParam("prix") float prix ) {
			
			if (nom.trim().length()<2 ||prix<0) {
				model.addAttribute("message", "ERREUR dans le formulaire");
				
				return "plat-formulaire";
			}
				else {
			
			platService.enregistrerFormulaire(nom,description,classification,prix);    //voir les donnes dans le log(service) pas ds BD
			platService.afficher(model);

			return "redirect:/plat";
			
			} 	
		}

		@RequestMapping(path="/remove")
		public String traiterFormulaire(@RequestParam int id) {
			logger.info("Delete dish id="+id);

			platService.supprimerRecette(id);
			return "redirect:/plat";
		}
	  
	    @Autowired
		public void setPlatService(PlatService platService) {
			this.platService = platService;
		}
		
		

	}


