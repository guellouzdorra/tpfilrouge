package fr.ib.dorra.Cuisine;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table (name="plats")
public class PlatCuisine implements Serializable {
	private int id;
	private String nom;
	private String description;
	private String classification;
	private float prix;
	
	
	
	public PlatCuisine() {
		this(null, null,null,0);
	}


	public PlatCuisine(String nom, String description, String classification, float prix) {
		this.id=0;
		this.nom = nom;
		this.description = description;
		this.classification = classification;
		this.prix = prix;
	}


	
	@Id
	@GeneratedValue(strategy =GenerationType.IDENTITY )
	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}

	@Column(length= 100,nullable = false, unique=false)
	public String getNom() {
		return nom;
	}


	public void setNom(String nom) {
		this.nom = nom;
	}

	@Column(length= 1000,nullable = false)
	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}

	@Column(length= 40,nullable = false, unique=false)
	public String getClassification() {
		return classification;
	}


	public void setClassification(String classification) {
		this.classification = classification;
	}

	@Column(nullable = false, precision=5, scale=2)
	public float getPrix() {
		return prix;
	}


	public void setPrix(float prix) {
		this.prix = prix;
	}


	@Override
	public String toString() {
		return id + " , " + nom + ", " +description+ ", "+classification + ", " +prix;
	}

}
