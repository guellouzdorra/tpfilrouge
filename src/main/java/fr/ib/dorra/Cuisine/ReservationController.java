package fr.ib.dorra.Cuisine;

import java.sql.Time;
import java.util.Date;
import java.util.List;

import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;



@RestController
public class ReservationController {
	private static final Logger logger = LoggerFactory.getLogger(Reservation.class);
	private SessionFactory sessionFactory;
    private ReservationService reservationService;
    @Autowired
    public void setCdService(ReservationService reservationService) {
    	this.reservationService = reservationService;
    }

    @RequestMapping(path="/json/reservation", method=RequestMethod.POST,consumes="application/json")
	public List<Reservation> traiterFormulaire(@RequestBody Reservation res ) {
    	System.out.println(res);
    	if (res.getNom().trim().length()<2 ||res.getNombreConvives()<=0) {
    	return null;
    	}else {

    	reservationService.enregistrerFormulaire(res);
    	return reservationService.afficher();

    }}
		
    @RequestMapping(path="/supprime")
	public void supprimerFormulaire(@RequestParam int id) {
		reservationService.supprimerReservation(id);
		

		
	}

}

