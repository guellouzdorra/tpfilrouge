package fr.ib.dorra.Cuisine;

import java.sql.Time;
import java.util.Date;
import java.util.List;

import javax.servlet.Registration;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

@Service
public class ReservationService {
    
    private SessionFactory sessionFactory;
    public void enregistrerFormulaire(Reservation res) {
        
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        session.save(res);
        tx.commit();
        session.close();    
    }
    
    public List<Reservation> afficher() {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        
        List<Reservation> reservation=session.
                createQuery("from Reservation order by date, horaire", Reservation.class).getResultList();
       
        session.close();
        return reservation;
    }
    
    public void supprimerReservation(int id) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        
        try {
            Reservation reservation = session.load(Reservation.class, id);
            session.delete(reservation);
            tx.commit();
        } catch (Exception e) {
            System.err.println("Erreur : "+e);
            tx.rollback();
        }
        session.close();
    }
    
    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
}
