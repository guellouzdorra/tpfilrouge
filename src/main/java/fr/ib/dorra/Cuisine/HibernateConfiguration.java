package fr.ib.dorra.Cuisine;

import java.util.Properties;

import org.hibernate.SessionFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Configuration
public class HibernateConfiguration {

	
	
	  @Bean
	  public SessionFactory sessionFactory() {
		  Properties options = new Properties();
		  options.put("hibernate.dialect","org.hibernate.dialect.MySQL5InnoDBDialect");
		  options.put("hibernate.connection.driver_class","com.mysql.jdbc.Driver");
		  options.put("hibernate.connection.url","jdbc:mysql://localhost/plats");
		  options.put("hibernate.connection.username","cuisinier");
		  options.put("hibernate.connection.password","password");
		  options.put("hibernate.hbm2ddl.auto","update");  
		  options.put("hibernate.show_sql","true");
		  
		  SessionFactory factory = new org.hibernate.cfg.Configuration().   
				  addProperties(options).  

		          addAnnotatedClass(PlatCuisine.class).
		          addAnnotatedClass(Reservation.class).    
		          buildSessionFactory();  

	      return factory;
	      

	  }
}

