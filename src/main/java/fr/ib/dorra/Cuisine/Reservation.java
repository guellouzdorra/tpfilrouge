package fr.ib.dorra.Cuisine;

import java.io.Serializable;
import java.sql.Time;
import java.time.LocalDateTime;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Reservation implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
private String nom;
private int nombreConvives;
private Date date;
private String horaire;
private int id;
public Reservation(String nom, int nombreConvives, Date date, String horaire) {
	super();
	this.nom = nom;
	this.nombreConvives = nombreConvives;
	this.date = date;
	this.horaire = horaire;
	this.id = 0;
} 

public Reservation() {
	super();
	this.nom = null;
	this.nombreConvives = 0;
	this.date = null;
	this.horaire = null;
	this.id = 0;
}
@Override
public String toString() {
	return "Reservation : " + nom + ", nombreConvives=" + nombreConvives + ", date=" + date + ", horaire=" + horaire
			+ "]";
}
public String getNom() {
	return nom;
}
public void setNom(String nom) {
	this.nom = nom;
}
public int getNombreConvives() {
	return nombreConvives;
}
public void setNombreConvives(int nombreConvives) {
	this.nombreConvives = nombreConvives;
}
public Date getDate() {
	return date;
}
public void setDate(Date date) {
	this.date = date;
}
public String getHoraire() {
	return horaire;
}
public void setHoraire(String horaire) {
	this.horaire = horaire;
}
public static long getSerialversionuid() {
	return serialVersionUID;
} 

@Id
@GeneratedValue(strategy =GenerationType.IDENTITY )
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}


}
